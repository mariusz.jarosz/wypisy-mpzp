Wtyczka do programu QGIS (testowana w wersji programu 3.2) służy do generowania wypisów z miejscowych planów zagospodarowania przestrzennego, dla gminy miejsko-wiejskiej.
pliki należy wypakować do  folderu  z wtyczkami dla programu QGIS (z reguły jest to C:\Users\NazwaUżytkowanika\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins      do folderu o nazwie wypis_mpzp
Do poprawnego funkcjonowania wtyczki wymagane jest aby w wersji Python'a dla programu QGIS była zainstalowana paczka python-docx-template. 
Integralną częścią wtyczki stanowi plik konfiguracyjny - config.json. Aby plik mógł być w łatwy sposób edytowany przez administratora został zapisany w formacie json. Znajduje się on w folderze z wtyczką i dla poprawnego funkcjonowania programu musi tam pozostać.
Plik konfiguracyjny dzieli się na 3 części:
1.	miasto, w którym znajdują się takie informacje jak :
<ul>
<li>nazwa warstwy z działkami dla miasta </li>
<li>nazwa szablonu docx, który będzie wykorzystywany  do generowania dokument,</li>
<li>nazwę folderu w  którym będą zapisywane gotowe wypisy,</li>
<li>nazwa grupy w programie QGIS z warstwami  z przeznaczeniem mpzp,</li>
<li>obręby - podzielone na: nazwę obrębu, nr obrębu i warstwę planu</li>
</ul>

2.	wies, w którym znajdują się takie informacje jak :
<ul>
<li>nazwa warstwy z działkami dla wsi,</li>
<li>obręby - podzielone na: nazwę obrębu/miejscowości, nr obrębu i warstwę planu i osobną nazwę grupy z warstwami planu dla każdej miejscowości</li></br>
!!! nazwy szablonów dla wsi są generowane na podstawie nazwy miejscowości
</ul>

3.	pozostałe, w którym znajdują się takie informacje jak :
<ul>
<li>nazwa pola z numerem działek do wyszukania w warstwie z działkami,</li>
<li>nazwa jaką będzie miała warstwa z wyszukanymi działkami,</li>
<li>numer kolumny z symbolami przeznaczenia w warstwie planu,</li>
<li>numer kolumny z opisem przeznaczenia w warstwie planu,</li>
<li>lokalizacja folderu z szablonami,</li>
lokalizacja folderu do którego mają być zapisywane wypisy (należy w nim dodatkowo utworzyć foldery z nazwami konkretnych wsi, które będą identyczne z nazwami obrębów
<li>procent od jakiego mają pojawiać się przeznaczenia , ponieważ są sytuacje, że czasem działki mają niewielkie przeznaczenie w danym terenie (rzędu 0.1 %)
i  nie chcemy, żeby były wypisywane.</li>
</ul>
UWAGA Ważne, żeby warstwy z mpzp i działek były zapisane w tym samym układzie współrzędnych.

Po skopiowaniu wtyczki powinna pojawić się nowa ikona, która umożliwi uruchomienie wtyczki (ewentualnie trzeba wejść w zakładkę Wtyczki - Zarządzanie wtyczkami i zaznaczyć Wypis MPZP).
OBSŁUGA:
Po uruchomieniu wtyczki należy wybrać, czy interesuje nas wyszukiwanie w mieście, czy wsi, następnie wybieramy interesujący obręb ( po dodaniu działek możliwość zmiany zostaje zablokowana ) i w pierwszym oknie wpisujemy nr działek oddzielone " , " (obręb zostanie dodany automatycznie) i naciskamy przycisk "Dodaj działki". Po dodaniu działek naciskamy "Wyszukaj" zostanie dodana nowa warstwa z wyszukanymi działkami , jeżeli działki nie występują zostanie wyświetlony odpowiedni komunikat. 

W oknach po prawej stronie należy podać:
<ul>
<li>znak sprawy,</li>
<li>dane wnioskodawcy w formie: </li>
</ul>
<ul>
	<li>Forma grzecznościowa</li>
	<li>Imię Nazwisko</li>
	<li>Adres</li>
	<li>Kod pocztowy i Miejscowość</li>
</ul>
np.:
<ul>
	<li>Pan</li>
	<li>Jan Kowalski</li>
	<li>ul. Wspólna 9</li>
	<li>00-000 Warszawa</li>
</ul>
<ul>
<li>zaznaczyć cel wypisu</li></br>
</ul>
Gdy wszystko jest już wypełnione/zaznaczone wciskamy przycisk "Ok" i program generuje wypis zgodnie z szablonem i znalezionymi danymi.</br>
TODO:
<ul>
<li>funkcja dodawania % powierzchni danego przeznaczenia (prawie ukończone)</li>
<li>generowanie wyrysów</li>
<li>lepsza kontrola nad błędami</li>
</ul>

Wtyczka, jak sam program QGIS, wydana jest na licencji  GNU GPL, każdy ma możliwość jej modyfikowania, prosiłbym jedynie o dodanie informacji o twórcy.</br>
Wtyczka była tworzona w ramach nauki programowania w języku Python, autor nie odpowiada za błędy wynikłe z jej użytkowania.</br>
W razie pytań proszę pisać na: mariusz.jarosz@protonmail.com</br>
Mariusz Jarosz




 
